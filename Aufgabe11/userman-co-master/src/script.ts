/**
 * script.ts  implements a browser-based user-manager
 */
abstract class AbstractUserList {

    /**
     * "User[]" contains a list of users, represented (here) as array
     */
    protected userList: User[]; // using array to store user list

    /**
     * constructor: creates an empty array and stores it in property 'userlist'
     */
    public constructor() { this.userList = []; }

    /**
     * getUser: return a list of all users as array
     * @returns array of all users
     */
    abstract getList() : User[]

    /**
     * addUser: adds a user to the user list
     * @param user  user to be added
     */
    abstract  addUser(user: User)

    /**
     * deletUser: deletes a user
     * @param userId  id of the user to be deleted - does not do anything, if userid does not exist
     */
    abstract  deleteUser(userId: number)

    /**
     * getUser: gets a user
     * @param userId  id of the user to be fetched.
     * @returns User to be fetched, null if userid not found
     */
    abstract  getUser(userId: number) : User

    /**
     * editUser: changes (edits) the properties of a user
     * @param userId  id of the user to be changed.
     * @param firstName  first name of user
     * @param lastName  last name of user
     * @returns User with changed properties, null if userid not found
     */
    abstract  editUser(userId: number, firstName: string, lastName: string) : User
}


// Aufgabe 11
class UserList extends AbstractUserList{
    public getList():User[]{
        return this.userList;
    }
    public addUser(user){
       this.userList.push(user);
    }
    public getUser(userId:number):User{
        // wenn die user id stimmt wird der user zurückgegeben
        for(let user of this.userList){
            if(user.id === userId){
                return user;
            }
        }
        // undefined für nicht existierenden benutzer
        return undefined;
    }
    public deleteUser(userId){
       //user durchsuchen und mit id vergleichen
        for(let user of this.userList){
            if(user.id === userId){
              const index = this.userList.indexOf(user);
              this.userList.splice(index,1);
            }
        }
    }
    public editUser(userId:number,firstName:string, lastName:string):User{
        for(let user of this.userList){
            if(user.id === userId){
                const index = this.userList.indexOf(user);
                this.userList[index].firstName = firstName;
                this.userList[index].lastName = lastName;
                return this.userList[index];
            }
        }
        return undefined;
    }


}


/*****************************************************************************
 * Class and enum declaration                                                *
 *****************************************************************************/
/**
 * User  Class representing a user
 */
class User {
    private static userCounter: number = 1;

    public id: number;
    public firstName: string;
    public lastName: string;
    public creationDate: Date;

    constructor(firstName: string, lastName: string, creationDate: Date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.creationDate = creationDate;
        this.id = User.userCounter++;
    }
}

/**
 * UserList  Class representing a user list - extends the AbstractUserList class
 */
// class UserList extends AbstractUserList{}

/**
 * "userList"  a global variable, storing the list of users
 */
 let userList: UserList = new UserList();
//let userList: User[] = [];


/*****************************************************************************
 * Event Handlers (callbacks)                                                *
 *****************************************************************************/

/**
 * addUser: add a user to the userlist
 * @param event  event that has been triggerd, needed to prevent default of form
 */
function addUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const firstNameField: JQuery = $('#add-first-name-input');
    const lastNameField: JQuery = $('#add-last-name-input');

    // Read values from input fields
    const firstName: string = firstNameField.val().toString().trim();
    const lastName: string = lastNameField.val().toString().trim();

    // Check if all required fields are filled in
    if (firstName && lastName) {
        // Create new user
        const user: User = new User(firstName, lastName, new Date());
        // Add user to user list
        userList.addUser(user);
        // Send response
        renderMessage('User created');
        addUserForm.trigger('reset');
        renderUserList();
    } else {
        renderMessage('Not all mandatory fields are filled in');
    }

}

/**
 * editUser: changes (edits) the properties of a user provided by the input fields of teh modal window
 * @param event  event that has been triggerd, needed to prevent default of form
 */
function editUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const editModal: JQuery = $('#edit-user-modal');
    const editUserForm: JQuery = $('#edit-user-form');
    const firstNameInput: JQuery = $('#edit-first-name-input');
    const lastNameInput: JQuery = $('#edit-last-name-input');
    const idHiddenInput: JQuery = $('#edit-id-input');

    // Read values from input fields
    const userId: number = Number(idHiddenInput.val().toString().trim());
    const firstName: string = firstNameInput.val().toString().trim();
    const lastName: string = lastNameInput.val().toString().trim();

    if (firstName && lastName) {
        // iterate through userList until user found (or end of list)
        for (const user of userList.getList()) {
            if (user.id === userId) {
                user.firstName = firstName;
                user.lastName = lastName;
                // Send success response
                renderMessage(`Successfully updated user ${user.firstName} ${user.lastName}`);
                editUserForm.trigger('reset');
                renderUserList();
                editModal.modal('hide');
                return;  // leave function when found
            }
        } // leave function when found
        // The user could not be found, send error response
        renderMessage('The user to update could not be found');
    } else {
        renderMessage('Not all mandatory fields are filled in');
    }
    editModal.modal('hide');
}

/**
 * deleteUser: deletes a user from the userlist
 * @param event  event that has been triggerd, needed to prevent default of form
 */
function deleteUser(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');

    // Create a new array without the user to delete
   userList.deleteUser(userId);

    renderMessage('User deleted');
    renderUserList();
}

/**
 * openEditUserModal: dopens a modal edit window, to allow editing the properties of a user
 * @param event  event that has been triggerd, needed to prevent default of form
 */
function openEditUserModal(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');

    // Define JQuery HTML objects
    const editUserModal: JQuery = $('#edit-user-modal');
    const editIdInput: JQuery = $('#edit-id-input'); // Hidden field for saving the user's id
    const editFirstNameInput: JQuery = $('#edit-first-name-input');
    const editLastNameInput: JQuery = $('#edit-last-name-input');

    // iterate through list until user found (or end of list)
    for (const user of userList.getList()) {
        if (user.id === userId) {
            // Fill in edit fields in modal
            editIdInput.val(user.id);
            editFirstNameInput.val(user.firstName);
            editLastNameInput.val(user.lastName);
            // Show modal
            editUserModal.modal('show');
            renderUserList();
            return;  // leave function when found
        }
    }
    renderMessage('The selected user can not be found');
}


/*****************************************************************************
 * Render functions                                                          *
 *****************************************************************************/

/**
 * renderMessage: renders a message, that is shown for some seconds
 * @param message  message to be shown
 */
function renderMessage(message: string) {
    // Define JQuery HTML Objects
    const messageWindow: JQuery = $('#messages');

    // Create new alert
    const newAlert: JQuery = $(`
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `);

    // Add message to DOM
    messageWindow.append(newAlert);

    // Auto-remove message after 5 seconds (5000ms)
    setTimeout(() => {
        newAlert.alert('close');
    }, 5000);
}

/**
 * renderUserList: renders the list of users as table
 */
function renderUserList() {
    // Define JQuery HTML objects
    const userTableBody: JQuery = $('#user-table-body');

    // Delete the old table of users from the DOM
    userTableBody.empty();
    // For each user create a row and append it to the user table
    for (const user of userList.getList()) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <tr>
                <td>${user.id}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>
                    <button class="btn btn-outline-dark btn-sm edit-user-button mr-4" data-user-id="${user.id}" >
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-outline-dark btn-sm delete-user-button" data-user-id="${user.id}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        `);

        // ... and append it to the table's body
        userTableBody.append(tableEntry);
    }
}

/*****************************************************************************
 * Main Callback: Wait for DOM to be fully loaded                            *
 *****************************************************************************/
$(() => {
    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const editUserForm: JQuery = $('#edit-user-form');
    const userTableBody: JQuery = $('#user-table-body');

    // Register listeners
    addUserForm.on('submit', addUser);
    editUserForm.on('submit', editUser);
    userTableBody.on('click', '.edit-user-button', openEditUserModal);
    userTableBody.on('click', '.delete-user-button', deleteUser);
});

