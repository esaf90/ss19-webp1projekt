//Taschenrechner
var Taschenrechner = /** @class */ (function () {
    // Konstruktor
    function Taschenrechner() {
        this.zahl1 = 0;
        this.zahl2 = 0;
    }
    //Methoden
    Taschenrechner.prototype.plus = function () {
        return this.zahl1 + this.zahl2;
    };
    Taschenrechner.prototype.minus = function () {
        return this.zahl1 - this.zahl2;
    };
    Taschenrechner.prototype.mal = function () {
        return this.zahl1 * this.zahl2;
    };
    Taschenrechner.prototype.geteilt = function () {
        return this.zahl1 / this.zahl2;
    };
    Taschenrechner.prototype.flaechenInhalt = function () {
        return this.mal();
    };
    Taschenrechner.prototype.umfang = function () {
        return this.mal();
    };
    Taschenrechner.prototype.potenzieren = function () {
    };
    Taschenrechner.prototype.flaechenInhaltKreis = function () {
        return Math.PI * this.mal(this.zahl1, this.zahl2);
    };
    Taschenrechner.prototype.umfangKreis = function () {
        return this.mal(2 * Math.PI, this.zahl2);
    };
    Taschenrechner.prototype.fakultaet = function () {
        var i;
        for (i = this.zahl1; i >= 1; i--) {
            this.zahl1 *= i;
        }
        return this.zahl1;
    };
    Taschenrechner.prototype.eulerberechnen = function () {
        console.log(this.zahl1);
        for (var i = 0; i <= this.zahl1; i++) {
            this.zahl1 = this.plus(zahl1, 1);
        }
        return this.zahl1;
    };
    Taschenrechner.prototype.modulo = function () {
        return this.zahl1 % this.zahl2;
    };
    Taschenrechner.prototype.ergebnisAnzeigen = function (ergebnis) {
        console.log('bbb');
        $("#ergebnistr").val(ergebnis);
    };
    return Taschenrechner;
}());
//Eventlistener
$(function () {
    var t1 = new Taschenrechner();
    $("#plus").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.plus();
        t1.ergebnisAnzeigen(erg);
    });
    $("#minus").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.minus();
        t1.ergebnisAnzeigen(erg);
    });
    $("#mal").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.mal();
        t1.ergebnisAnzeigen(erg);
    });
    $("#geteilt").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.geteilt();
        t1.ergebnisAnzeigen(erg);
    });
    $("#flaechenInhalt").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.flaechenInhalt();
        t1.ergebnisAnzeigen(erg);
    });
    $("#umfang").on("click", function () {
        t1.zahl1 = 2 * Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.umfang();
        t1.ergebnisAnzeigen(erg);
    });
    $("#flaechenInhaltKreis").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.flaechenInhaltKreis();
        t1.ergebnisAnzeigen(erg);
    });
    $("#umfangkreis").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.umfangKreis();
        t1.ergebnisAnzeigen(erg);
    });
    $("#potenzieren").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.potenzieren();
        t1.ergebnisAnzeigen(erg);
    });
    $("#fakultaet").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.fakultaet();
        t1.ergebnisAnzeigen(erg);
    });
    $("#eulerscheZahl").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.eulerberechnen();
        t1.ergebnisAnzeigen(erg);
    });
    $("#modulo").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        var erg = t1.modulo();
        t1.ergebnisAnzeigen(erg);
    });
});
