//Taschenrechner
class Taschenrechner{
    //Attribute
    zahl1: number;
    zahl2: number;

    // Konstruktor
    constructor(){
        this.zahl1=0;
        this.zahl2=0;
    }

    //Methoden
    plus(){
        return this.zahl1 + this.zahl2;
    }

    minus(){
        return this.zahl1 - this.zahl2;
    }

    mal(){
        return this.zahl1 * this.zahl2;
    }

    geteilt(){
        return this.zahl1 / this.zahl2;
    }

    flaechenInhalt(){
        return this.mal(this.zahl1,this.zahl2);
    }

    umfang(){
        return this.mal(2*this.zahl1,this.zahl2);
    }

    potenzieren(){

    }

    flaechenInhaltKreis(){
        return Math.PI*this.mal(this.zahl1,this.zahl2);
    }

    umfangKreis(){
        return this.mal(2*Math.PI,this.zahl2);
    }

    fakultaet(){
        let i: number;
        for (i = this.zahl1; i >=1;i--) {
            this.zahl1 *= i;
        }
        return this.zahl1;
    }

     eulerberechnen(){
        console.log(this.zahl1);
        for(let i:number = 0;i <= this.zahl1;i ++){
            this.zahl1 = this.plus(zahl1,1);
        }
        return this.zahl1;
    }

     modulo(){
        return this.zahl1 % this.zahl2;
    }

     ergebnisAnzeigen(ergebnis:number){
         console.log('bbb');

         $("#ergebnistr").val(ergebnis);
    }
}

//Eventlistener
$(() => {

    let t1=new Taschenrechner();

    $("#plus").on("click", function () {
        console.log('aaa');
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.plus();
        t1.ergebnisAnzeigen(erg);
    });

    $("#minus").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.minus();
        t1.ergebnisAnzeigen(erg);
    });

    $("#mal").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.mal();
        t1.ergebnisAnzeigen(erg);
    });

    $("#geteilt").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.geteilt();
        t1.ergebnisAnzeigen(erg);
    });

    $("#flaechenInhalt").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.flaechenInhalt();
        t1.ergebnisAnzeigen(erg);
    });

    $("#umfang").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.umfang();
        t1.ergebnisAnzeigen(erg);
    });

    $("#flaechenInhaltKreis").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.flaechenInhaltKreis();
        t1.ergebnisAnzeigen(erg);
    });

    $("#umfangkreis").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.umfangKreis();
        t1.ergebnisAnzeigen(erg);
    });

    $("#potenzieren").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.potenzieren();
        t1.ergebnisAnzeigen(erg);
    });

    $("#fakultaet").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.fakultaet();
        t1.ergebnisAnzeigen(erg);
    });

    $("#eulerscheZahl").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.eulerberechnen();
        t1.ergebnisAnzeigen(erg);
    });

    $("#modulo").on("click", function () {
        t1.zahl1 = Number($("#zahl1").val());
        t1.zahl2 = Number($("#zahl2").val());
        let erg = t1.modulo();
        t1.ergebnisAnzeigen(erg);
    });
});