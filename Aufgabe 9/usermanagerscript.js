$(function () {
    var put = $("#put");
    var bhinzu = $("#bhinzu");
    var tab = $("#tab");
    var save = $("#save");
    var editsbutt = $("<button type='button' class='btn btn-primary editsbutt' data-toggle='modal' data-target='#jumpscare'> Edit</button>");
    var moputput = $("#moputput");
    bhinzu.on("click", function () {
        var eintrag = $("<tr><td>" + put.val() + "</td></tr>");
        var editsbutt = editsbutt.clone();
        editsbutt.on("click", function (event) {
            $(event.target.parentElement).toggleClass("neuername");
        });
        eintrag.append(editsbutt);
        tab.append(eintrag);
        put.val(" ");
    });
    save.on("click", function () {
        $(".neuername").find("td").text(moputput.val());
        $(".neuername").toggleClass("neuername");
    });
});
